using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using practica2_0.Models;

namespace practica2_0.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }
         public IActionResult Uriel()
    {
        ViewBag.Nombre = "Francisco Uriel";
        ViewBag.Apellidos = "Guerrero Rivera";
        ViewBag.Edad = 21;
        ViewBag.Correo = "franciscoguerrero@utez.edu.mx";
        ViewBag.Telefono = "777-189-6334";
        return View();
    }


    public IActionResult Aldo()
    {
        ViewBag.Nombres = "Aldo";
        ViewBag.Apellidos = "Hernández Ortiz";
        ViewBag.Edad = 20;
        ViewBag.Correo = "20193tn145@utez.edu.mx";
        ViewBag.Telefono = "7771256715";
        return View("Aldo");
    }

    public IActionResult Math()
    {
        ViewBag.Nombre = "Mathew";
        ViewBag.Apellidos = "Cordourier Rojas";
        ViewBag.Edad = 20;
        ViewBag.Telefono = "777-556-23-40";
        ViewBag.Correo = "20193tn106@utez.edu.mx";
        return View("Math");
    }

    public IActionResult Manuel()
    {
        ViewBag.Nombre = "Manuel";
        ViewBag.Apellido = "Aldana Molina";
        ViewBag.Edad = "21";
        ViewBag.Correo = "20193tn126@utez.edu.mx";
        ViewBag.Telefono = "7774852010";
        return View("Manuel");
    }
    public IActionResult Valerie()
    {
        ViewBag.Nombre ="Valerie Gisel Martinez Romero";
        ViewBag.Correo = "20193tn153@utez.edu.mx";
        ViewBag.Telefono = "777-511-85-98";
        return View("Valerie");
    }
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
